#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void maxnum(int num,int *max)
{
    if (num > *max)
        *max = num;
}
void minnum(int num, int *min)
{
    if (num < *min)
        *min = num;
}
double average(int nums)
{
    double av = nums / 25.0;
    printf ("average = %f \n",av);
}
int main()
{
    srand(time(NULL));
    int max = -101;
    int min = 101;
    int line[25];
    double av = 0.0;
    for (int i = 0; i < 25; i++)
    {
        line[i] = rand()% (100 - (-100)) + (-100);
        maxnum(line[i], &max);
        minnum(line[i], &min);
        av += line[i];
        printf("%i ", line[i]);
    }
    puts("");
    printf("max = %i \n", max);
    printf("min = %i \n", min);
    average(av);
    return 0;
}